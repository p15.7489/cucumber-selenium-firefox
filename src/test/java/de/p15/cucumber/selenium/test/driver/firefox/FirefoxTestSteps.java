package de.p15.cucumber.selenium.test.driver.firefox;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class FirefoxTestSteps {
	private FirefoxDriver driver;

	@Before()
	public void init() {
		WebDriverManager.firefoxdriver().setup();
		FirefoxOptions options = new FirefoxOptions();
		options.addArguments("--headless");
		options.addArguments("--disable-gpu");
		driver = new FirefoxDriver(options);
	}

	@Given("I am on the search page {string}")
	public void i_am_on_the_search_page(String string) {
		driver.get(string);
		WebElement element = driver.findElement(By.id("L2AGLb"));
		element.click();
	}

	@When("I search for {string}")
	public void i_search_for(String string) {
		WebElement element = driver.findElement(By.name("q"));
		element.sendKeys(string);
		element.submit();
	}

	@Then("the page title should start with {string}")
	public void the_page_title_should_start_with(String string) {
		new WebDriverWait(driver, Duration.ofSeconds(10)).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver d) {
				return d.getTitle().toLowerCase().startsWith(string);
			}
		});
	}

	@After()
	public void closeBrowser() {
		driver.quit();
	}
}
